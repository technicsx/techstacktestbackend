import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { ApartmentEntity } from "../entity/entity.apartment";

export default class CreateApartments implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<void> {
    await factory(ApartmentEntity)().createMany(4);
  }
}
