import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("apartment")
export class ApartmentEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("int")
  rooms: number;

  @Column({ type: "varchar", length: 99 })
  name: string;

  @Column("float")
  price: number;

  @Column({ type: "varchar", length: 999 })
  description: string;
}
