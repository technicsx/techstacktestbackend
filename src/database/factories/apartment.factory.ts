import { faker } from "@faker-js/faker";
import { define } from "typeorm-seeding";
import { ApartmentEntity } from "../entity/entity.apartment";

define(ApartmentEntity, () => {
  const apartment: ApartmentEntity = new ApartmentEntity();

  apartment.name = faker.lorem.word();
  apartment.rooms = Math.floor(randomFloatNum(1, 5));
  apartment.description = faker.lorem.lines(2);
  apartment.price = parseFloat(randomFloatNum(200,500).toFixed(2));

  return apartment;
});

function randomFloatNum(min: number, max: number): number {
  return Math.random() * (max - min + 1) + min;
}
