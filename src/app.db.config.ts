import {
  TypeOrmModuleOptions,
} from "@nestjs/typeorm";
import ormconfig from "./database/ormconfig";

export const typeOrmConfig: TypeOrmModuleOptions = ormconfig;

module.exports = typeOrmConfig;
