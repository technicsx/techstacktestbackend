import {
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from "@nestjs/common";
import { ApartmentService } from "./apartment.service";
import { Body } from "@nestjs/common/decorators";
import { ApartmentsQueryParamsDto } from "./dto/apartment.dto.query";
import { CreateApartmentDto } from "./dto/apartment.dto.create";
import { UpdateApartmentDto } from "./dto/apartment.dto.update";
import { HttpStatus } from "@nestjs/common/enums";
import { HttpException } from "@nestjs/common/exceptions";

@Controller("apartments")
export class ApartmentController {
  constructor(private readonly apartmentService: ApartmentService) {}

  @Get()
  async getApartments(@Query() queryParams: ApartmentsQueryParamsDto) {
    return await this.apartmentService.getAll(queryParams);
  }

  @Get(":id")
  async getApartment(@Param("id") id: string) {
    const apartment = await this.apartmentService.getOne(id);
    if (apartment) {
      return apartment;
    }
    throw new HttpException(
      "No such apartment was found",
      HttpStatus.NOT_FOUND,
    );
  }

  @Post()
  async addApartment(@Body() apartmentToAdd: CreateApartmentDto) {
    return await this.apartmentService.add(apartmentToAdd);
  }

  @Put(":id")
  async updateApartment(
    @Param("id") id: string,
    @Body() apartmentToUpdate: UpdateApartmentDto,
  ) {
    try {
      return await this.apartmentService.update(id, apartmentToUpdate);
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.BAD_REQUEST,
        { cause: error },
      );
    }
  }

  @Delete(":id")
  async deleteApartment(@Param("id") id: string) {
    try {
      return await this.apartmentService.remove(id);
    } catch (error) {
      throw new HttpException(
        error.message,
        HttpStatus.BAD_REQUEST,
        { cause: error },
      );
    }
  }
}
