import { Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { ApartmentEntity } from "../database/entity/entity.apartment";
import { InjectRepository } from "@nestjs/typeorm";
import { ApartmentsQueryParamsDto } from "./dto/apartment.dto.query";
import { UpdateApartmentDto } from "./dto/apartment.dto.update";
import { CreateApartmentDto } from "./dto/apartment.dto.create";

@Injectable()
export class ApartmentService {
  constructor(
    @InjectRepository(ApartmentEntity)
    private readonly apartmentRepository: Repository<ApartmentEntity>,
  ) {}

  async add(apartmentDto: CreateApartmentDto): Promise<ApartmentEntity> {
    return this.apartmentRepository.save(apartmentDto);
  }

  async update(id: string, apartmentDTO: UpdateApartmentDto): Promise<any> {
    const apartment = await this.getOne(id);

    if (apartment) {
      let propsToUpdate: any = {};
      if (apartmentDTO.price && apartmentDTO.price !== apartment.price) {
        propsToUpdate.price = apartmentDTO.price;
      }
      if (apartmentDTO.name && apartmentDTO.name !== apartment.name) {
        propsToUpdate.name = apartmentDTO.name;
      }
      if (
        apartmentDTO.description &&
        apartmentDTO.description !== apartment.description
      ) {
        propsToUpdate.description = apartmentDTO.description;
      }
      if (apartmentDTO.rooms && apartmentDTO.rooms !== apartment.rooms) {
        propsToUpdate.rooms = apartmentDTO.rooms;
      }

      return this.apartmentRepository.update({ id }, { ...propsToUpdate });
    }
    throw new Error("No such apartment to update");
  }

  async remove(id: string): Promise<any> {
    const apartment = await this.getOne(id);
    if (apartment) {
      return await this.apartmentRepository.delete({ id });
    }
    throw new Error("No such apartment to delete");
  }

  async getOne(id: string): Promise<ApartmentEntity> {
    return this.apartmentRepository.findOne({ id });
  }

  async getAll(
    queryParams: ApartmentsQueryParamsDto,
  ): Promise<ApartmentEntity[]> {
    let options: any = {};
    if (queryParams.price) {
      options.order = { price: queryParams.price.toUpperCase() };
    }
    if (queryParams.rooms) {
      options.where = { rooms: queryParams.rooms };
    }
    return this.apartmentRepository.find(options);
  }
}
