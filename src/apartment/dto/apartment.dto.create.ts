import { IsNumber, IsOptional, IsPositive, IsString, MaxLength } from '@nestjs/class-validator';
import { Transform } from 'class-transformer';


export class CreateApartmentDto {
  @Transform(({ value }) => parseInt(value))
  @IsNumber()
  @IsPositive()
  rooms: number;

  @IsString()
  @MaxLength(99)
  name: string;

  @Transform(({ value }) => parseFloat(value))
  @IsNumber()
  @IsPositive()
  price: number;

  @IsOptional()
  @IsString()
  @MaxLength(999)
  description: string;
}
