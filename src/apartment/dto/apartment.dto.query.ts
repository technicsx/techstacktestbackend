import { IsIn, IsOptional, IsNumber } from "@nestjs/class-validator";
import { Transform } from "class-transformer";

export class ApartmentsQueryParamsDto {
  @IsOptional()
  @Transform(({ value }) => value.toLowerCase())
  @IsIn(["asc", "desc"])
  price?: string;

  @IsOptional()
  @Transform(({ value }) => parseInt(value))
  @IsNumber()
  rooms?: number;
}
