import { IsOptional } from '@nestjs/class-validator';
import { CreateApartmentDto } from './apartment.dto.create';

export class UpdateApartmentDto extends CreateApartmentDto {
  @IsOptional()
  rooms: number;

  @IsOptional()
  name: string;

  @IsOptional()
  price: number;
}
