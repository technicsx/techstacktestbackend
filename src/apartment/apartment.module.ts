import { Module } from '@nestjs/common';
import { ApartmentController } from './apartment.controller';
import { ApartmentService } from './apartment.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApartmentEntity } from '../database/entity/entity.apartment';

@Module({
  imports: [
    TypeOrmModule.forFeature([ApartmentEntity]),
  ],
  providers: [ApartmentService],
  controllers: [ApartmentController],
  exports: [],
})
export class ApartmentModule {}
