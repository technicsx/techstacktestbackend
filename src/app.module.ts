import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { ApartmentModule } from "./apartment/apartment.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import ormconfig from "./database/ormconfig";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: () => ormconfig,
    }),
    ApartmentModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
