<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

# Techstack Test Task : Backend part

## .env file example:
```
APP_PORT=some_app_port
POSTGRES_PORT=some_db_port
POSTGRES_HOST=some_db_host
POSTGRES_USER=some_db_username
POSTGRES_PASSWORD=some_db_password
POSTGRES_DB=some_db_database_name
```

## To run it successfully such commands needed:
```
npm run start:db
npm run start:db:seed:run
npm run start:dev (if in dev) -- or -- npm run start:prod (if in prod)
```
